// Create clients and set shared const values outside of the handler.

// Get the DynamoDB table name from environment variables
const tableName = process.env.SAMPLE_TABLE;

// Create a DocumentClient that represents the query to add an item
const dynamodb = require('aws-sdk/clients/dynamodb');
const docClient = new dynamodb.DocumentClient({region: 'eu-north-1', endpoint: 'http://172.17.0.2:8000'});

function queryTable(params){
  return new Promise((resolve,reject) => {
    docClient.query(params, (err, data) => {
      if(err){
        console.log(err)
        reject()
      }else{
        let dataArray = data.Items.map(x => {return JSON.parse(x.data)})
        if(dataArray.length === 0) 
          reject()
        else
          resolve({lastEvaluatedKey: data.LastEvaluatedKey, dataArray: dataArray})
      }
    })
  })
  
}
exports.getByIdHandler = async (event) => {
  if (event.httpMethod !== 'GET') {
    throw new Error(`getMethod only accept GET method, you tried: ${event.httpMethod}`);
  }

  const body = JSON.parse(event.body);
  const id = event.pathParameters.id;
  const lastEvaluatedKey = body.lastEvaluatedKey ? body.lastEvaluatedKey : undefined

  if(isNaN(id)){
    return {
      statusCode: 400, 
      body: JSON.stringify({code: "400", message: "Id not a number"})
    }
  }

  let params = {
    TableName: tableName,
    KeyConditionExpression: "#id = :id",
    ExpressionAttributeNames:{
        "#id": "id"
    },
    ExpressionAttributeValues: {
        ":id": id
    },
    ExclusiveStartKey: lastEvaluatedKey,
    Limit: 5
  }

  let response = {}
  await queryTable(params).then(data => {
    response.statusCode = 200
    response.body = {
      requestId: event.requestContext.requestId,
      data: data.dataArray,  
      paging: {
        next: data.lastEvaluatedKey
      }
    }
  })
  .catch(() => {
    response.statusCode = 500
    response.body = {code: "500", message: `Something went wrong or orders not found with id ${id}`}
  })

  response.body = JSON.stringify(response.body)
  return response;
  
}
