// Create clients and set shared const values outside of the handler.

const AWS = require('aws-sdk');
const dynamo = new AWS.DynamoDB({region: 'eu-north-1', endpoint: 'http://172.17.0.2:8000'});

// Get the DynamoDB table name from environment variables
const tableName = process.env.SAMPLE_TABLE;
 
function createTable () {
    const params = {
        TableName: tableName,
        AttributeDefinitions: [
            // required attributes to be used as keys
            {
                AttributeName: 'id',
                AttributeType: 'S'
            },
            {
                AttributeName: 'data',
                AttributeType: 'S'
            }
        ],
        KeySchema: [
            {
                AttributeName: 'id',
                KeyType: 'HASH'
            },
            {
                AttributeName: 'data',
                KeyType: 'RANGE'
            }
        ],

        ProvisionedThroughput: {
        ReadCapacityUnits: 5, // Eventually Consistent Model
        WriteCapacityUnits: 5
        }
    }

    return new Promise((resolve, reject) => {
        dynamo.createTable(params, (err, data) => {
        if(err) reject(err);
        else resolve(data);
        })
    })
}

function checkTableExists(table){
    let params = {
        TableName: table
    };
    return new Promise((resolve, reject) => {
        dynamo.describeTable(params, (err, data) => {
        if(err)
            reject(err);
        else
            resolve(data);
        })
    })
}

exports.putItemHandler = async (event) => {
    if (event.httpMethod !== 'POST') {
        throw new Error(`postMethod only accepts POST method, you tried: ${event.httpMethod} method.`);
    }

    const body = JSON.parse(event.body);
    const id = event.pathParameters.id;

    if(isNaN(id) || !validBody(body)){
        return {
            statusCode: 400, 
            body: JSON.stringify({code: "400", message: "Invalid POST request"})
        }
    }

    await checkTableExists(tableName).then(() => {}).catch(async () => {
        await createTable().then(() => {}).catch(() => {})
    })

    let params = {
        TableName : tableName,
        Item: {
            id: {S: id.toString()},
            data: {S: JSON.stringify(body)}
        }
    };

    let response = {}
    await dynamo.putItem(params).promise()
    .then(() => {
        response.statusCode = 200
        response.body = {
            requestId: event.requestContext.requestId,
            data: body
        }
    }).catch(() => {
        response.statusCode = 500
        response.body = {code: "500", message: `Something went wrong`}
    })

    response.body = JSON.stringify(response.body)
    return response;
    
};

function validBody(body){
    const example = {
        "shopname": "string",
        "orderNumber": "string",
        "createdAt": "string",
        "price": 0,
        "productId": "string"
    }
    
    if(!body ||typeof body != "object")
        return false
    console.log("tyyppi", typeof body)
    let valid = true
    Object.keys(example).forEach(key => {
        if(!body[key] || typeof body[key] != typeof example[key])
            valid = false
    })
    return valid
}
